#-----provider-----
aws_profile = "academy2"
aws_region  = "eu-west-2"

#-------VPC--------
vpc_cidr = "10.0.0.0/16"
cidrs = {
  publicM  = "10.0.1.0/24"
  public2  = "10.0.2.0/24"
  private1 = "10.0.3.0/24"
  private2 = "10.0.4.0/24"
  rds1     = "10.0.5.0/24"
  rds2     = "10.0.6.0/24"
  rds3     = "10.0.7.0/24"
}

cidr_blocks = ["82.24.141.235/32", "86.4.90.40/32", "86.125.62.70/32", "82.24.122.149/32"]

zone_id = "Z05618473MYN7NTHS8ITT"

#--------ELB-----------
elb_healthy_threshold   = "2"
elb_unhealthy_threshold = "2"
elb_timeout             = "3"
elb_interval            = "30"

#--------DNS-------------
domain_name = "trio.academy.grads.al-labs"

#--------Instance Default-------------
instance_type_j = "t3.large"
instance_type_a = "t2.micro"
ami             = "ami-01a6e31ac994bbc09"
key             = "trioKey"

#-------Instance Bastion--------
ami_bastion = "ami-01a6e31ac994bbc09"


#-------S3 Bucket--------
s3_bucket = "trio-s3-bucket"

#------SSH Key---------
generate_ssh_key = true #If set to true, new SSH key is generated
